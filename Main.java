import org.testng.annotations.Test;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println(Main.userLogin("user1", "XXXXXXXXXXX"));
        /*System.out.println(Main.createUser("\"Programmer\"",
                "\"John\"",
                "\"James\"",
                "\"john@email.com\"",
                "\"12345\"",
                "\"12345\"",
                1));*/ // code 500
        //System.out.println(Main.deleteUser("user1")); //code 500
        System.out.println(Main.userLogout());
        System.out.println(Main.getUserByUsername("user1"));
    }

    public static int createUser(String username,
                                 String firstName,
                                 String lastName,
                                 String email,
                                 String password,
                                 String phone,
                                 int userStatus) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
        String jsonInputString = "{\"username\": " + username + ", " +
                "\"firstName\": " + firstName + ", " +
                "\"lastName\": " + lastName + ", " +
                "\"email\": " + email + ", " +
                "\"password\": " + password + ", " +
                "\"phone\": " + phone + ", " +
                "\"userStatus\": " + userStatus + "}";

        var request = HttpRequest.newBuilder(
                URI.create("https://petstore3.swagger.io/api/v3/user/"))
                .POST(HttpRequest.BodyPublishers.ofString(jsonInputString))
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.statusCode();
    }

    public static int getUserByUsername (String username) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
        var request = HttpRequest.newBuilder(
                URI.create("https://petstore3.swagger.io/api/v3/user/" + username))
                .GET()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.statusCode();
    }

    public static int userLogin(String login, String password) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
        var request = HttpRequest.newBuilder(
                URI.create("https://petstore3.swagger.io/api/v3/user/login?username=" +
                       login + "&password=" + password))
                .GET()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.statusCode();
    }

    public static int deleteUser(String username) throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
        var request = HttpRequest.newBuilder(
                URI.create("https://petstore3.swagger.io/api/v3/user/" + username))
                .DELETE()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.statusCode();
    }

    public static int userLogout() throws IOException, InterruptedException {
        HttpClient httpClient = HttpClient.newHttpClient();
        var request = HttpRequest.newBuilder(
                URI.create("https://petstore3.swagger.io/api/v3/user/logout"))
                .GET()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();
        var response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.statusCode();
    }
}
