import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.lang.annotation.Target;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Tests {

    @Test
    void testUserLogin1() throws IOException, InterruptedException {
        if (Main.userLogin("user1", "XXXXXXXXXXX") == 200)
            assertTrue(true);
        else
            assertFalse(false);
    }

    @Test
    void testUserLogin2() throws IOException, InterruptedException {
        if (Main.userLogin("AAAAAAAAAAAAA", "OOOOOOOOOO") != 200)
            assertTrue(true);
        else
            assertFalse(false);
    }

    @Test
    void testGetUserByName1() throws IOException, InterruptedException {
        if (Main.getUserByUsername("user1") == 200)
            assertTrue(true);
        else
            assertFalse(false);
    }

    @Test
    void testGetUserByName2() throws IOException, InterruptedException {
        if (Main.getUserByUsername("KtoKtoNikto") != 200)
            assertTrue(true);
        else
            assertFalse(false);
    }

    @Test
    void testUserLogout() throws  IOException, InterruptedException {
        if (Main.userLogout() == 200)
            assertTrue(true);
        else
            assertFalse(false);
    }
}
